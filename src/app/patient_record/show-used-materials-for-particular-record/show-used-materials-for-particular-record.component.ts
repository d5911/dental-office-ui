import { Component, OnInit } from '@angular/core';
import { FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import {  MaterialResponseDTO } from 'src/app/materials/material';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-show-used-materials-for-particular-record',
  templateUrl: './show-used-materials-for-particular-record.component.html',
  styleUrls: ['./show-used-materials-for-particular-record.component.css']
})
export class ShowUsedMaterialsForParticularRecordComponent implements OnInit {

  id: string;
  materials: MaterialResponseDTO[] = [];
  firstName: string;
  lastName: string;
  date: string;

  constructor(private recordService: RecordService, private router: Router) { 
    this.id = this.router.getCurrentNavigation()?.extras.state?.['recordId'];
    this.firstName = this.router.getCurrentNavigation()?.extras.state?.['firstName'];
    this.lastName = this.router.getCurrentNavigation()?.extras.state?.['lastName'];
    this.date = this.router.getCurrentNavigation()?.extras.state?.['date']
  }

  ngOnInit(): void {
   this.recordService.getMaterialsByRecordId(this.id)
    .subscribe(materialsResponse => {
      this.materials = materialsResponse;
    }); 
  }

  goBackToListOfRecords() {
    this.router.navigate(['/listOfRecords']);
  }
}
