import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { MaterialResponseDTO } from "../materials/material";
import { Service } from "../services/service";
import { Record, RecordRequest, RecordResponse } from "./record";

@Injectable({
    providedIn: 'root'
  })
  export class RecordService {
  
    private apiUrl = 'http://localhost:8080/record';
    
   
    constructor(private http:HttpClient) {
    }
  
    create(record: RecordRequest): Observable<Record> {
      return this.http.post<Record>(this.apiUrl, record);
    }
  
    findAll(searchTerm: string = "", pageNumber: number = 0, pageSize?: number, sortKey?: string, sortAsc?: boolean): Observable<RecordResponse> {
      if(sortAsc == undefined) {
        sortAsc = true;
      }
      if(sortKey == "date" || sortKey == "") {
        return this.http.get<RecordResponse>(
          this.apiUrl + 
          "?searchTerm=" + searchTerm +
          "&pageNo=" + (Number.isInteger(pageNumber) ? pageNumber : "") +
          "&pageSize=" + (pageSize ? pageSize : "") +
          "&sort=" + (sortKey ? sortKey : "") +
          "&sortAsc=" + (sortAsc)
        );
      } else {
        return this.http.get<RecordResponse>(
          this.apiUrl + 
          "?searchTerm=" + searchTerm +
          "&pageNo=" + (Number.isInteger(pageNumber) ? pageNumber : "") +
          "&pageSize=" + (pageSize ? pageSize : "") +
          "&sort=patient." + (sortKey ? sortKey : "") +
          "&sortAsc=" + (sortAsc)
        );
      } 
      
    }

    get(id: string): Observable<Record> {
      return this.http.get<Record>(this.apiUrl + '/' + id);
    }

    getMaterialsByRecordId(recordId: string): Observable<MaterialResponseDTO[]> {
      // const url = `${this.apiUrl}/${recordId}/materials`;
      return this.http.get<MaterialResponseDTO[]>(this.apiUrl + "/" + recordId + "/materials");
    }

    getServicesByRecordId(recordId: string): Observable<Service[]> {
      const url = `${this.apiUrl}/${recordId}/services`;
      return this.http.get<Service[]>(url); 
    }
  
    // delete(id: string): Observable<object> {
    //   return this.http.delete(
    //     this.apiUrl + '/' + id
    //   );
    // }
  
    // get(id: number): Observable<Appointment> {
    //   return this.http.get<Appointment>(this.apiUrl + '/' + id);
    // }
  
    // edit(id: number, appointment: Appointment): Observable<Appointment> {
    //   return this.http.put<Appointment>(this.apiUrl + '/' + id, appointment);
    // }  
  }