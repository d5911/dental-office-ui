import { FormArray, FormGroup } from "@angular/forms";
import { Material } from "../materials/material";
import { Patient } from "../patients/patient";
import { ServiceService } from "../services/service.service";

export interface Record {
    id?: string;
    date: string;
    patient: Patient;
    materials: Material[];
    services: ServiceService[];
    description: string;
}

export interface RecordResponse {
    content: Record[];
    totalPages: number;
}

export interface RecordRequest {
    patientId: string;
    description: string;
    materialIds: string[];
    serviceIds: string[];
    numberOfUsedMaterials: Number[];
}

export interface PatientForRecord {
    patientId: string;
    firstName: string;
    lastName: string;
}