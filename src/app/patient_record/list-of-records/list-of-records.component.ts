import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Record } from '../record';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-list-of-records',
  templateUrl: './list-of-records.component.html',
  styleUrls: ['./list-of-records.component.css']
})
export class ListOfRecordsComponent implements OnInit {

  records: Record[] = [];
  searchTerm: string = "";
  pageNumber = 1;
  pageSize = 10;
  totalPages: number = 0;
  isErrorToastShown = false;
  id: string;
  sortAsc = true;

  constructor(private recordService: RecordService, private router: Router) {
    this.id = this.router.getCurrentNavigation()?.extras.state?.['recordId'];
   }

  ngOnInit(): void {
    this.recordService.findAll()
      .subscribe(recordResponse => {
        this.records = recordResponse.content;
        this.totalPages = recordResponse.totalPages;
      });
  }

  getPatientFirstName(record: Record) : string {
    return record.patient.firstName;
  }

  getPatientLastName(record: Record) : string {
    return record.patient.lastName;
  }

  getDate(record: Record) : string {
    return record.date;
  }

  getDescription(record: Record) : string {
    return record.description;
  }

  showUsedMaterials(id: string | undefined, firstName: string, lastName: string, date: string) {
    this.router.navigate(['/showUsedMaterialsForParticularRecord'], {state: {recordId: id, firstName: firstName, lastName: lastName, date: date}})
  }

  showServices(id: string | undefined, firstName: string, lastName: string, date: string) {
    this.router.navigate(['/showServicesForParticularRecord'], {state: {recordId: id, firstName: firstName, lastName: lastName, date: date}})
  }

  findAllFiltered($event: any) {
    this.recordService.findAll($event.target.value)
      .subscribe(recordResponse => {
        this.records = recordResponse.content;
        this.totalPages = recordResponse.totalPages;
      }); 
  }

  findWidthPaging(selectedPage: any) {
    if(Number.isInteger(selectedPage)) {
      this.recordService.findAll(this.searchTerm, this.pageNumber-1, this.pageSize)
        .subscribe(recordResponse => this.records = recordResponse.content);
    }
  }

  onSort(sortKey: string) {
    this.sortAsc = !this.sortAsc;

    this.recordService.findAll(this.searchTerm, this.pageNumber-1, this.pageSize, sortKey, this.sortAsc)
      .subscribe(recordResponse => this.records = recordResponse.content);
  }

}
