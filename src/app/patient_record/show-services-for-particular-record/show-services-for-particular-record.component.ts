import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from 'src/app/services/service';
import { RecordService } from '../record.service';

@Component({
  selector: 'app-show-services-for-particular-record',
  templateUrl: './show-services-for-particular-record.component.html',
  styleUrls: ['./show-services-for-particular-record.component.css']
})
export class ShowServicesForParticularRecordComponent implements OnInit {

  id: string;
  services: Service[] = [];
  firstName: string;
  lastName: string;
  date: string;

  constructor(private recordService: RecordService, private router: Router) { 
    this.id = this.router.getCurrentNavigation()?.extras.state?.['recordId'];
    this.firstName = this.router.getCurrentNavigation()?.extras.state?.['firstName'];
    this.lastName = this.router.getCurrentNavigation()?.extras.state?.['lastName'];
    this.date = this.router.getCurrentNavigation()?.extras.state?.['date'];
  }

  ngOnInit(): void {
    this.recordService.getServicesByRecordId(this.id)
    .subscribe(servicesResponse => {
      this.services = servicesResponse;
    }); 
  }

  goBackToListOfRecords() {
    this.router.navigate(['/listOfRecords']);
  }

}
