import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Material } from '../materials/material';
import { Service } from '../services/service';
import { MaterialService } from '../materials/material.service';
import { ServiceService } from '../services/service.service';
import { RecordService } from '../patient_record/record.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PatientForRecord, RecordRequest } from '../patient_record/record';

@Component({
  selector: 'app-adding-materials-and-services-to-the-patient-record',
  templateUrl: './adding-materials-and-services-to-the-patient-record.component.html',
  styleUrls: ['./adding-materials-and-services-to-the-patient-record.component.css']
})
export class AddingMaterialsAndServicesToThePatientRecordComponent implements OnInit {

  materials: Material[] = [];
  services: Service[] = [];

  patientForRecord: PatientForRecord;

  pageNumberMaterials = 1;
  pageNumberServices = 1;
  pageSize = 10;
  totalPages: number = 0;
  sortAsc = true;

  dataForRecord: FormGroup;

  searchTermMaterials: string;
  searchTermServices: string;

  constructor(private formBuilder: FormBuilder, private materialService: MaterialService, private serviceService: ServiceService, private recordService: RecordService, private router: Router) {
    const navigationState = this.router.getCurrentNavigation()?.extras.state;

    this.patientForRecord = {
      patientId: navigationState?.['patientId'],
      firstName: navigationState?.['firstName'],
      lastName: navigationState?.['lastName']
    }

    this.dataForRecord = this.formBuilder.group({
      teethDescription:  ['', [Validators.required, Validators.minLength(6), Validators.pattern(/^[a-zA-Z0-9\s.,!?'"()]*$/)]],
      searchTermMaterials: [''],
      searchTermServices: [''],
      materialsForm: this.formBuilder.array([]),
      servicesForm: this.formBuilder.array([])
    });

    this.searchTermMaterials = this.dataForRecord.get('searchTermMaterials')?.value;
    this.searchTermServices = this.dataForRecord.get('searchTermServices')?.value;
  }

  ngOnInit(): void {
    this.materialService.findAll()
      .subscribe(materialResponse => {
        this.materials = materialResponse.content;
        this.totalPages = materialResponse.totalPages;
      });
    this.serviceService.findAll()
      .subscribe(serviceResponse => {
        this.services = serviceResponse.content;
        this.totalPages = serviceResponse.totalPages;
      });
  }

  findAllMaterialsFiltered($event: any) {
    this.materialService.findAll($event.target.value)
      .subscribe(materialResponse => {
        this.materials = materialResponse.content;
        this.totalPages = materialResponse.totalPages;
      }); 
    
  }

  findAllServicesFiltered($event: any) {
    this.serviceService.findAll($event.target.value)
      .subscribe(serviceResponse => {
        this.services = serviceResponse.content;
        this.totalPages = serviceResponse.totalPages;
      });
  }


  onSortMaterials(sortKey: string) {
    this.sortAsc = !this.sortAsc;
        
    this.materialService.findAll(this.searchTermMaterials, this.pageNumberMaterials-1, this.pageSize, sortKey, this.sortAsc)
        .subscribe(materialResponse => this.materials = materialResponse.content);  
  }

  onSortServices(sortKey: string) {
    this.sortAsc = !this.sortAsc;

      this.serviceService.findAll(this.searchTermServices, this.pageNumberServices-1, this.pageSize, sortKey, this.sortAsc)
      .subscribe(serviceResponse => this.services = serviceResponse.content);
  }

  findMaterialsWidthPaging(selectedPage: any) {
    if(Number.isInteger(selectedPage)) {
      this.materialService.findAll(this.searchTermMaterials, this.pageNumberMaterials-1, this.pageSize)
        .subscribe(materialResponse => this.materials = materialResponse.content);
    }
  }

  findServicesWidthPaging(selectedPage: any) {
    if(Number.isInteger(selectedPage)) {
      this.serviceService.findAll(this.searchTermServices, this.pageNumberServices-1, this.pageSize)
        .subscribe(serviceResponse => this.services = serviceResponse.content);
    }
  }

  addMaterialsForPatientRecord(id: string, materialName: string) {   
    (this.materialsFormArray).push(this.formBuilder.group({
      materialId: this.formBuilder.control(id),
      materialName: this.formBuilder.control(materialName),
      numberOfUsedMaterials: this.formBuilder.control(1)
    }));
  }

  addServicesForPatientRecord(id: string, serviceName: string) {
    (this.dataForRecord.get('servicesForm') as FormArray).push(this.formBuilder.group({
      serviceId: this.formBuilder.control(id),
      serviceName: this.formBuilder.control(serviceName)
    }))
  }

  removeMaterial(index: number) {
    if (this.materialsFormArray.length && index >= 0 && index < this.materialsFormArray.length) {
      this.materialsFormArray.removeAt(index);
    }
  }

  removeService(index: number) {
    const servicesForm = this.dataForRecord.get('servicesForm') as FormArray;
    if (servicesForm.length && index >= 0 && index < servicesForm.length) {
      servicesForm.removeAt(index);
    }
  }

  getMaterialIds(): string[] {
    const materialIds: string[] = [];

    for (let i = 0; i < this.materialsFormArray.length; i++) {
      const control = this.materialsFormArray.at(i) as FormGroup;
      materialIds.push(control.get('materialId')?.value);
    }
  
    return materialIds;
  }

  getNumberOfUsedMaterials(): Number[] {
    const numberOfUsedMaterialsList: Number[] = [];

    for(let i = 0; i < this.materialsFormArray.length; i++) {
      const control = this.materialsFormArray.at(i) as FormGroup;
      numberOfUsedMaterialsList.push(control.get('numberOfUsedMaterials')?.value);
    }

    return numberOfUsedMaterialsList;
  }

  getServiceIds(): string[] {
    const serviceIds: string[] = [];

    for(let i = 0; i < this.servicesFormArray.length; i++) {
      const control = this.servicesFormArray.at(i) as FormGroup;
      serviceIds.push(control.get('serviceId')?.value);
    }

    return serviceIds;
  }

  addNewRecord(): void {
    const dataToBeAdded : RecordRequest = {
      patientId: this.patientForRecord.patientId,
      description: this.dataForRecord.get('teethDescription')?.value,
      materialIds: this.getMaterialIds(),
      serviceIds: this.getServiceIds(),
      numberOfUsedMaterials: this.getNumberOfUsedMaterials()
    }
    this.recordService.create(dataToBeAdded).subscribe({
      next: () => {
        this.router.navigate(['/listOfRecords']);
      },
      error:(error) => {
      console.error("error", error)
      }
    });
  }

  getNumberOfUsedMaterialsControl(i: number): FormControl {
    return this.dataForRecord.get(['materialsForm', i, 'numberOfUsedMaterials']) as FormControl
  }

  get materialsFormLength(): number {
    return  this.materialsFormArray.length;
  }

  get materialsFormArray(): FormArray {
    return this.dataForRecord.get('materialsForm') as FormArray;
  }

  get servicesFormArray(): FormArray {
    return this.dataForRecord.get('servicesForm') as FormArray;
  }

  get servicesFormLength(): number {
    return this.servicesFormArray.length;
  }
}