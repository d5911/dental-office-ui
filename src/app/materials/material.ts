export interface Material {
    id?: string;
    materialName: string;
    quantity: number;
}

export interface MaterialResponse {
    content: Material[];
    totalPages: number;
}

export class MaterialResponseDTO {
    id?: string;
    materialName: string = '';
}

export class AddedMaterials {
    id?: string;
    numberOfUsedMaterials!: number;
}

export interface MaterialRequest {
    materialName: string;
    quantity: string;
}