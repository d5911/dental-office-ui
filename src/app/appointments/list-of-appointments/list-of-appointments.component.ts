import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Appointment } from '../appointment';
import { AppointmentService } from '../appointment.service';

@Component({
  selector: 'app-list-of-appointments',
  templateUrl: './list-of-appointments.component.html',
  styleUrls: ['./list-of-appointments.component.css']
})
export class ListOfAppointmentsComponent implements OnInit {

  appointments: Appointment[] = [];
  searchTerm: string = "";
  pageNumber = 1;
  pageSize = 10;
  totalPages: number = 0;
  sortAsc = true;

  constructor(private appointmentService: AppointmentService, private router: Router) { }
  
  ngOnInit(): void {
    this.appointmentService.findAll()
      .subscribe(appointmentResponse => {
        this.appointments = appointmentResponse.content;
        this.totalPages = appointmentResponse.totalPages;
      });
  }

  findAllFiltered($event: any) {
    this.appointmentService.findAll($event.target.value)
      .subscribe(appointmentResponse => {
        this.appointments = appointmentResponse.content;
        this.totalPages = appointmentResponse.totalPages;
      }); 
  }

  findWidthPaging(selectedPage: any) {
    if(Number.isInteger(selectedPage)) {
      this.appointmentService.findAll(this.searchTerm, this.pageNumber-1, this.pageSize)
        .subscribe(appointmentResponse => this.appointments = appointmentResponse.content);
    }
  }

  onDelete(id?: string){
    this.appointmentService.delete(id!).subscribe(() => {
      this.appointmentService.findAll()
        .subscribe((appointments) => {
          this.appointments = appointments.content
        });
    });
  }

  goToEditPage(id: string | undefined): void {
    this.router.navigate(['/editAppointment'], {state: { appointmentId: id }});
  }

  onSort(sortKey: string) {
    this.sortAsc = !this.sortAsc;

    this.appointmentService.findAll(this.searchTerm, this.pageNumber-1, this.pageSize, sortKey, this.sortAsc)
      .subscribe(appointmentResponse => this.appointments = appointmentResponse.content);
  }
}