
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Service, ServiceResponse } from './service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
private apiUrl = 'http://localhost:8080/dentalService';
  
    constructor(private http:HttpClient) {
     }
  
    create(service: Service): Observable<Service> {
      return this.http.post<Service>(this.apiUrl, service);
    }
  
    findAll(searchTerm: string = "", pageNumber: number = 0, pageSize?: number, sortKey?: string, sortAsc?: boolean): Observable<ServiceResponse> {
      if(sortAsc == undefined) {
        sortAsc = true;
      }
      return this.http.get<ServiceResponse>(
        this.apiUrl + 
        "?searchTerm=" + searchTerm +
        "&pageNo=" + (Number.isInteger(pageNumber) ? pageNumber : "") +
        "&pageSize=" + (pageSize ? pageSize : "") + 
        "&sort=" +  (sortKey ? sortKey : "") +
        "&sortAsc=" + (sortAsc)
        );
    }

    get(id: number): Observable<Service> {
      return this.http.get<Service>(this.apiUrl + '/' + id);
    }

    delete(id: string): Observable<object> {
      return this.http.delete(
        this.apiUrl + '/' + id
      );
    }

    edit(id: number, service: Service): Observable<Service> {
      return this.http.put<Service>(this.apiUrl + '/' + id, service);
    }  
}