import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth.service';
import { LoginService } from '../login.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  loginForm = new FormGroup ({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })

  constructor(private service: LoginService, private router: Router, private authService: AuthService, private userService: UserService) { }

  ngOnInit(): void {
  }

  login(){
    this.service.login(this.loginForm.value).subscribe((response: any) => {
      if(response.token) {
        localStorage.setItem('jwttoken', response.token);
        this.router.navigate(['/homePage'])
        const role = this.userService.getUserRole();
        this.authService.updateRole(role);
      } else {
        alert(response.message);
      }
    })
  }
}
