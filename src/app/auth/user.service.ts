import { Injectable } from '@angular/core';

import { throwError } from 'rxjs';
import { UserRole } from './auth.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private jwtService: JwtService) { }

  getUserRole(): UserRole {
    const token = localStorage.getItem('jwttoken');
    if (token) {
      const decodedToken = this.jwtService.decodeToken(token);
      return decodedToken ? decodedToken.role : throwError(() => new Error('Invalid token'));
    } else {
      throw new Error('Token not found');
    }
  }
}
