import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  login(data: any) {
    let url = 'http://localhost:8080/auth/authenticate';
    return this.http.post(url, data);
  }
}
