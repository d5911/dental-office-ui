import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export enum UserRole{
  ROLE_DOCTOR = 'ROLE_DOCTOR',
  ROLE_NURSE = 'ROLE_NURSE'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentRole = new BehaviorSubject<UserRole | undefined>(undefined);

  updateRole(role: UserRole) {
    this.currentRole.next(role);
  }

  getRole(): Observable<UserRole | undefined> {
    return this.currentRole.asObservable();
  }
}
