import { Component, OnInit } from '@angular/core';
import { AuthService, UserRole } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  role: UserRole | undefined;

  constructor(private authService: AuthService) {
    this.authService.getRole().subscribe((role)=> {
      this.role = role;    
    })
  }

  ngOnInit(): void {}

}
